export interface coinMarketDao {
  id: string;
  price_usd: string;
  rank: string;
  h_24_volume_cny: string;
  h_24_volume_usd:string
  available_supply: string;
  last_updated: string;
  market_cap_cny:string;
  market_cap_usd:string;
  max_supply:string;
  name:string;
  percent_change_1h:string;
  percent_change_7d:string;
  percent_change_24h:string;
  price_btc:string;
  price_cny:string;
  symbol:string;
  total_supply:string;
}