import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import {CoinMarketApiService} from '../service/coinMarketAdapter/coin-market-api.service';
import {SharedCoinInfoService} from '../service/shared-coin-info.service';
import {HttpModule} from '@angular/http';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable/src';
import { CoinTableComponent } from './nav/coin-table/coin-table.component';
import { ExchangePriceCompareComponent } from './nav-on-coin-detail/exchange-price-compare/exchange-price-compare.component';
import {AppRoutingModule} from './app-routing.module';
import { NavOnCoinDetailComponent } from './nav-on-coin-detail/nav-on-coin-detail.component';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import * as highcharts from 'highcharts/highstock';
import { TrendingChartComponent } from './nav-on-coin-detail/trending-chart/trending-chart.component';
import { CoinDetailComponent } from './nav-on-coin-detail/coin-detail/coin-detail.component';
import { BizhidaoAdCardComponent } from './nav/coin-table/bizhidao-ad-card/bizhidao-ad-card.component';
import { CoinDetailHeaderComponent } from './nav-on-coin-detail/coin-detail-header/coin-detail-header.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    CoinTableComponent,
    ExchangePriceCompareComponent,
    NavOnCoinDetailComponent ,
    TrendingChartComponent,
    CoinDetailComponent,
    BizhidaoAdCardComponent,
    CoinDetailHeaderComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    NgxDatatableModule,
    HttpModule,
    AppRoutingModule,
    ChartModule,
  ],
  providers: [
  CoinMarketApiService,
  SharedCoinInfoService,
  NavComponent,
  { provide: HighchartsStatic,
    useValue: highcharts
  },],
  bootstrap: [AppComponent]
})
export class AppModule { }
