import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CoinMarketApiService } from '../../../service/coinMarketAdapter/coin-market-api.service';


let mockExchangeData = [{ "exchange_name": "Bittrex", "price": 215.56269279999995, "rank": "1", "volume_per": "37.82%", "volume_24h": 394065417.1999999, "market_name": "NEO/BTC" }, { "exchange_name": "Bitfinex", "price": 215.20662799999997, "rank": "2", "volume_per": "21.00%", "volume_24h": 218809126.89999998, "market_name": "NEO/USD" }, { "exchange_name": "Binance", "price": 216.6076367, "rank": "3", "volume_per": "16.68%", "volume_24h": 173764936.79999998, "market_name": "NEO/BTC" }, { "exchange_name": "Bittrex", "price": 215.3408166, "rank": "4", "volume_per": "9.37%", "volume_24h": 97591648.69999999, "market_name": "NEO/USDT" }, { "exchange_name": "Bitfinex", "price": 216.61295109999998, "rank": "5", "volume_per": "6.62%", "volume_24h": 69020769.99999999, "market_name": "NEO/BTC" }, { "exchange_name": "Coinnest", "price": 214.95419399999994, "rank": "6", "volume_per": "2.70%", "volume_24h": 28094309.879999995, "market_name": "NEO/KRW" }, { "exchange_name": "Bittrex", "price": 216.69731719999996, "rank": "7", "volume_per": "2.01%", "volume_24h": 20922460.65, "market_name": "NEO/ETH" }, { "exchange_name": "Bitfinex", "price": 215.997145, "rank": "8", "volume_per": "1.17%", "volume_24h": 12166588.069999998, "market_name": "NEO/ETH" }, { "exchange_name": "Binance", "price": 214.39883919999997, "rank": "9", "volume_per": "1.08%", "volume_24h": 11264136.52, "market_name": "NEO/ETH" }, { "exchange_name": "BCEX", "price": 213.7577897, "rank": "10", "volume_per": "0.70%", "volume_24h": 7302782.759999999, "market_name": "ANS/BTC" }, { "exchange_name": "Kucoin", "price": 216.13465509999997, "rank": "11", "volume_per": "0.23%", "volume_24h": 2443999.5579999997, "market_name": "NEO/BTC" }, { "exchange_name": "HitBTC", "price": 217.55758569999995, "rank": "12", "volume_per": "0.18%", "volume_24h": 1922211.8369999998, "market_name": "NEO/BTC" }, { "exchange_name": "Cryptopia", "price": 215.1827132, "rank": "13", "volume_per": "0.15%", "volume_24h": 1515819.6689999998, "market_name": "NEO/BTC" }, { "exchange_name": "Kucoin", "price": 231.18570019999999, "rank": "14", "volume_per": "0.14%", "volume_24h": 1495445.5879999998, "market_name": "RPX/NEO" }, { "exchange_name": "Kucoin", "price": 222.62951619999996, "rank": "15", "volume_per": "0.04%", "volume_24h": 412861.78569999995, "market_name": "NEO/ETH" }, { "exchange_name": "HitBTC", "price": 224.75394759999995, "rank": "16", "volume_per": "0.04%", "volume_24h": 400454.65459999995, "market_name": "NEO/ETH" }, { "exchange_name": "HitBTC", "price": 225.10801949999995, "rank": "17", "volume_per": "0.03%", "volume_24h": 322376.8184, "market_name": "NEO/USDT" }, { "exchange_name": "Kucoin", "price": 230.1919074, "rank": "18", "volume_per": "0.02%", "volume_24h": 192511.48279999997, "market_name": "GAS/NEO" }, { "exchange_name": "Livecoin", "price": 225.99353139999994, "rank": "19", "volume_per": "0.01%", "volume_24h": 124726.9751, "market_name": "NEO/BTC" }, { "exchange_name": "Allcoin", "price": 213.7577897, "rank": "20", "volume_per": "0.01%", "volume_24h": 66423.75557999998, "market_name": "NEO/BTC" }, { "exchange_name": "Cryptopia", "price": 223.28385169999996, "rank": "21", "volume_per": "0.00%", "volume_24h": 20376.871059999998, "market_name": "NEO/LTC" }, { "exchange_name": "Livecoin", "price": 232.50499999999997, "rank": "22", "volume_per": "0.00%", "volume_24h": 11113.27399, "market_name": "NEO/USD" }, { "exchange_name": "Cryptopia", "price": 243.49717209999997, "rank": "23", "volume_per": "0.00%", "volume_24h": 7379.177259999999, "market_name": "NEO/DOGE" }, { "exchange_name": "Cryptomate", "price": 201.44100339999997, "rank": "24", "volume_per": "0.00%", "volume_24h": 0.0, "market_name": "NEO/GBP" }]

@Component({
  selector: 'app-exchange-price-compare',
  templateUrl: './exchange-price-compare.component.html',
  styleUrls: ['./exchange-price-compare.component.css']
})
export class ExchangePriceCompareComponent implements OnInit {

  rows = mockExchangeData;
  columns = [
    { prop: 'exchange_name', name: '交易所' },
    { prop: 'price' , name: '价格'},
    // { prop: 'volume_per',name: '交易量' },
    { prop: 'market_name', name: '交易对' },    
  ];

  constructor(
    private activatedRoute: ActivatedRoute, 
    private coinMarketApiService: CoinMarketApiService,
  ) {
    mockExchangeData.forEach(coin => {
      coin.price = Number( coin.price.toFixed(2));
    });
   }

  ngOnInit() {
    // this.getCoinDetail()
  }

  getCoinDetail(): void {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.activatedRoute.snapshot.paramMap);
    this.coinMarketApiService.getCoinDetail(id)
      .subscribe(exchangeData => {
        this.rows = exchangeData
      });
  }
}
