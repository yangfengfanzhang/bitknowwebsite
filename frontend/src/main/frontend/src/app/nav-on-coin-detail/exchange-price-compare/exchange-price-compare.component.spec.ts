import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExchangePriceCompareComponent } from './exchange-price-compare.component';

describe('ExchangePriceCompareComponent', () => {
  let component: ExchangePriceCompareComponent;
  let fixture: ComponentFixture<ExchangePriceCompareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExchangePriceCompareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangePriceCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
