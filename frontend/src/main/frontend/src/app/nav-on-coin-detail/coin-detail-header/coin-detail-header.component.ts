import { Component } from '@angular/core';
import { SharedCoinInfoService } from '../../../service/shared-coin-info.service'
import { coinMarketDao } from '../../../DAO/coin.market.dao';

@Component({
  selector: 'app-coin-detail-header',
  templateUrl: './coin-detail-header.component.html',
  styleUrls: ['./coin-detail-header.component.css']
})
export class CoinDetailHeaderComponent  {
  marketcapdata: coinMarketDao;
  
  constructor(private _sharedCoinInfoService: SharedCoinInfoService) { 
    this._sharedCoinInfoService.getMarketcapdata().subscribe(data => {
      this.marketcapdata = data;
    })
  }

}
