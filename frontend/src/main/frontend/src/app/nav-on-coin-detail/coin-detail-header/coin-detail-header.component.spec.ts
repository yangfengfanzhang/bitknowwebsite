import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoinDetailHeaderComponent } from './coin-detail-header.component';

describe('CoinDetailHeaderComponent', () => {
  let component: CoinDetailHeaderComponent;
  let fixture: ComponentFixture<CoinDetailHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoinDetailHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoinDetailHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
