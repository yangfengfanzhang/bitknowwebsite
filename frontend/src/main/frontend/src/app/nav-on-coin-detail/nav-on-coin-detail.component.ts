import { Component} from '@angular/core';
import { ChartModule } from 'angular2-highcharts'; 
import { Http } from '@angular/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nav-on-coin-detail',
  templateUrl: './nav-on-coin-detail.component.html',
  styleUrls: ['./nav-on-coin-detail.component.css']
})
export class NavOnCoinDetailComponent  {

  coinId: string;
  coinFromService: string;
  baseUrl: string;
  

  constructor(
    private activatedRoute: ActivatedRoute,
  ) {
  }
}
