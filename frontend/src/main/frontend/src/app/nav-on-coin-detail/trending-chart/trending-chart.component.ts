import { Component, OnInit } from '@angular/core';
import { ChartModule } from 'angular2-highcharts';
import { Http } from '@angular/http';

@Component({
  selector: 'app-trending-chart',
  templateUrl: './trending-chart.component.html',
  styleUrls: ['./trending-chart.component.css']
})
export class TrendingChartComponent implements OnInit {

  options: Object;

  constructor(private http: Http) {
    http.get('https://cdn.rawgit.com/gevgeny/angular2-highcharts/99c6324d/examples/aapl.json').subscribe(res => {
      this.options = {
        // chart: {
        //   reflow: true          
        // },
        title: { text: 'eos price' },
        series: [{
          name: 'EOS',
          data: res.json(),
          tooltip: {
            valueDecimals: 2
          }
        }]
      };
    });
  }


  ngOnInit() {
  }

}
