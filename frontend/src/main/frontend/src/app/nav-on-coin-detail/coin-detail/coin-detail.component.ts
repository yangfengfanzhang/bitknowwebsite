import { Component } from '@angular/core';
import { SharedCoinInfoService } from '../../../service/shared-coin-info.service'
import { coinMarketDao } from '../../../DAO/coin.market.dao';

@Component({
  selector: 'app-coin-detail',
  templateUrl: './coin-detail.component.html',
  styleUrls: ['./coin-detail.component.css']
})
export class CoinDetailComponent {
  marketcapdata: coinMarketDao;

  constructor(private _sharedCoinInfoService: SharedCoinInfoService) {
    this._sharedCoinInfoService.getMarketcapdata().subscribe(data => {
      this.marketcapdata = data;
    })

  }
}
