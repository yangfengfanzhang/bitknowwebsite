import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavOnCoinDetailComponent } from './nav-on-coin-detail.component';

describe('NavOnCoinDetailComponent', () => {
  let component: NavOnCoinDetailComponent;
  let fixture: ComponentFixture<NavOnCoinDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavOnCoinDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavOnCoinDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
