import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoinTableComponent } from './nav/coin-table/coin-table.component';
import { ExchangePriceCompareComponent } from './nav-on-coin-detail/exchange-price-compare/exchange-price-compare.component';
import { NavOnCoinDetailComponent } from './nav-on-coin-detail/nav-on-coin-detail.component';
import { TrendingChartComponent } from './nav-on-coin-detail/trending-chart/trending-chart.component';
import {CoinDetailComponent} from './nav-on-coin-detail/coin-detail/coin-detail.component';

const routes: Routes = [
  { path: '行情', component: CoinTableComponent },
  { path: '', component: CoinTableComponent },
  // { path: 'currency/:id', component: ExchangePriceCompareComponent},  
  {
    path: '交易所差价/:id',
    component: NavOnCoinDetailComponent,
    children: [
      {
        path: '',
        redirectTo: 'exchange',
        pathMatch: 'full'
      },
      {
        path: 'exchange',
        component: ExchangePriceCompareComponent,
        // outlet:"navDetail"
      },
      {
        path: 'trending',
        component: TrendingChartComponent,
        // outlet:"navDetail"
      },
      {
        path: 'detail',
        component: CoinDetailComponent,
        // outlet:"navDetail"
      }]
  },
  // { path: '交易所差价/:id/exchange', component: ExchangePriceCompareComponent, outlet: 'navDetail' },
  // { path: '交易所差价/:id/trending', component: TrendingChartComponent, outlet: 'navDetail' },
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],

})
export class AppRoutingModule { }
