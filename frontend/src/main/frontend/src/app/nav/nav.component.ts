import { Component, OnInit , NgModule} from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  menu: any;
  menuCN: any;
  coinName: string;
  constructor() {
    this.menu =[
      {'menuName': 'Announcement', 'menuList': ['normal', 'pro']},
      {'menuName': 'Pricing', 'menuList': ['ICO analysis', 'Push Notifications']},
      // {'menuName': 'Market', 'menuList': ['coinmarketcap', 'Bitfiniex', 'Poloniex', 'Liqui', 'CoinCheck', 'Gdax', 'Bitstamp']},
    ];

    this.menuCN = [
      {'menuName': '行情'},
      // {'menuName': '区块链知识'},
      {'menuName': '抖抖表演'},           
    ]
  }

  ngOnInit() {
  }




}
