import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BizhidaoAdCardComponent } from './bizhidao-ad-card.component';

describe('BizhidaoAdCardComponent', () => {
  let component: BizhidaoAdCardComponent;
  let fixture: ComponentFixture<BizhidaoAdCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BizhidaoAdCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BizhidaoAdCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
