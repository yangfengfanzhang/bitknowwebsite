import { Component, OnInit, ViewChild } from '@angular/core';
import { CoinMarketApiService } from '../../../service/coinMarketAdapter/coin-market-api.service';
import { DatatableComponent } from '../../../../node_modules/@swimlane/ngx-datatable/src/components/datatable.component';
import { RouterModule, Router } from '@angular/router';
import { DecimalPipe } from '@angular/common';
import {coinMarketDao} from '../../../DAO/coin.market.dao';
import {SharedCoinInfoService} from '../../../service/shared-coin-info.service'
import { from } from 'rxjs/observable/from';
import {Page} from '../../../DAO/Page';

@Component({
  selector: 'app-coin-table',
  templateUrl: './coin-table.component.html',
  styleUrls: ['./coin-table.component.css']
})
export class CoinTableComponent implements OnInit {
  page = new Page();  
  selected = [];
  rows: coinMarketDao [] = [];
  selectedCoin: coinMarketDao;
  riseOrFall: boolean;
  temp = [];
  pageA = 1;
  pageB = 2;
  pageC = 3;
  showFirstPageButton = false;
  // calculate later on
  lastPage = 100;

  @ViewChild(DatatableComponent) table: DatatableComponent;


  constructor(
    private coinMarketApiService: CoinMarketApiService,
    private router: Router,
    private _sharedCoinInfoService: SharedCoinInfoService,
  ) {
    this.coinMarketDataTransform();
    this.page.pageNumber = 0;
    this.page.size = 100;
  }

  ngOnInit() {
  }

  coinMarketDataTransform() {
    this.coinMarketApiService.getMarketCapData().subscribe(marketcapdata => {
      this.rows = marketcapdata;
      // json = JSON.parse(JSON.stringify(json).split('"_id":').join('"id":'));
      this.rows = JSON.parse(JSON.stringify(this.rows).split('"24h_volume_usd"').join('"h_24_volume_usd"'));     
      this.rows = JSON.parse(JSON.stringify(this.rows).split('"24h_volume_cny"').join('"h_24_volume_cny"'));            
      this.temp = [...marketcapdata];
      this.rows.forEach(data => {
        data.price_cny = parseFloat(data.price_cny).toFixed(2);
      });
    });
  }

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    // this.table.rowDetail.toggleExpandRow(row);
  }

  onSelect({ selected }) {
    let selectedId = selected[0].id;    
    this.constructCoinMarketDao(selectedId);    
    console.log(this.selectedCoin);
    this._sharedCoinInfoService.publishMarketcapdata(this.selectedCoin);    
    this._sharedCoinInfoService.publishPageNumber(this.pageB);
    this._sharedCoinInfoService.changeMessage("nan");    
    this.router.navigate(['./交易所差价/' + this.selected.pop()['id']]);        
  }

  constructCoinMarketDao(selectedId: string){
    this.selectedCoin = this.rows.filter(row => row.id === selectedId)[0];
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.symbol.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
  

  retrievePageA(pageNum) {
    this.coinMarketApiService.updateUrl(pageNum);
    this.coinMarketDataTransform();
    if (pageNum !== 1) {
      this.pageA = this.pageA - 1;
      this.pageB = this.pageA + 1;
      this.pageC = this.pageB + 1;
    }
  }

  retrievePageC(pageNum) {
    this.coinMarketApiService.updateUrl(pageNum);
    this.coinMarketDataTransform();
    if (pageNum !== this.lastPage) {
      this.pageA = this.pageA + 1;
      this.pageB = this.pageA + 1;
      this.pageC = this.pageB + 1;
      this.showFirstPageButton = true;
    }
  }

  retrievePageB(pageNum) {
    this.coinMarketApiService.updateUrl(pageNum);
    this.coinMarketDataTransform();
    this.showFirstPageButton = true;
  }

  retrievePage1() {
    this.coinMarketApiService.updateUrl(1);
    this.coinMarketDataTransform();
    this.pageA = 1;
    this.pageB = this.pageA + 1;
    this.pageC = this.pageB + 1;
    this.showFirstPageButton = false;
  }

  checkChange(value){
    return value.includes('-')
  }

  setPage(pageInfo){
    console.log(pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.coinMarketApiService.updateUrl(this.page.pageNumber);
    this.coinMarketDataTransform();
  }

}
