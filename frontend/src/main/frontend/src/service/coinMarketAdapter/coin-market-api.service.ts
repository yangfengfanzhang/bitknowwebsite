import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

const exchangePriceApiUrl = 'https://api.bitknows.com/market/';

@Injectable()
export class CoinMarketApiService {

  // private baseCoinMarketCapUrl = '/price';
  // private coinMarketCapUrl = '/price?pageNum=1';
  private baseCoinMarketCapUrl = 'https://api.coinmarketcap.com/v1/ticker/?convert=CNY&limit=100';
  private coinMarketCapUrl = 'https://api.coinmarketcap.com/v1/ticker/?convert=CNY&limit=100';
  constructor(
    private http: Http
  ) { }

  getMarketCapData() {
    return this.http.get(this.coinMarketCapUrl)
      .map(res => res.json());
  }

  updateUrl(pageNum) {
    // this.coinMarketCapUrl = this.baseCoinMarketCapUrl + '?pageNum=' + pageNum;
    this.coinMarketCapUrl = this.coinMarketCapUrl;
    console.log(this.coinMarketCapUrl);
  }

  getCoinDetail(coinDetail) {
    let coinUrl = exchangePriceApiUrl + coinDetail;
    console.log(coinUrl)
    return this.http.get(coinUrl)
      .map(res => res.json());
  }

}
