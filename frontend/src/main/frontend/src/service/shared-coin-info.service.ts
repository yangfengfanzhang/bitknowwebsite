import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs';
import { coinMarketDao } from '../DAO/coin.market.dao';


@Injectable()
export class SharedCoinInfoService {

  private messageSource = new BehaviorSubject<string>("default message");
  private marketcapdata = new BehaviorSubject<coinMarketDao>(
    {
      id: "",
      price_usd: "",
      rank: "",
      h_24_volume_cny: "",
      h_24_volume_usd: "",
      available_supply: "",
      last_updated: "",
      market_cap_cny: "",
      market_cap_usd: "",
      max_supply: "",
      name: '',
      percent_change_1h: '',
      percent_change_7d: '',
      percent_change_24h: '',
      price_btc: '',
      price_cny: '',
      symbol: '',
      total_supply: ''
    }
  );
  private pageNum = new BehaviorSubject<number>(1);

  constructor() {
    console.log('CONSTRUCTING SERVICE');
  }

  publishMarketcapdata(coin: coinMarketDao) {
    this.marketcapdata.next(coin);
  }

  getMarketcapdata(): Observable<coinMarketDao> {
    return this.marketcapdata.asObservable();
  }

  changeMessage(message: string) {
    this.messageSource.next(message);
  }

  getMessageObservable(): Observable<string> {
    return this.messageSource.asObservable();
  }

  publishPageNumber(pageN: number) {
    this.pageNum.next(pageN);
  }

  getPageNumber(): Observable<number> {
    return this.pageNum.asObservable();
  }
}
