package com.bitknow.ng2boot.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.web.bind.annotation.PathVariable;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bitknow.ng2boot.model.UserDetails;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    public List getUserDetails() {
        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(UserDetails.class);
        return criteria.list();
    }

    @Transactional
    public void save(UserDetails user) {
        entityManager.persist(user);
    }

}
