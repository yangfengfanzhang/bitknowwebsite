package com.bitknow.ng2boot.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.bitknow.ng2boot.model.CoinInfo;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CoinInfoDao {

    @PersistenceContext
    private EntityManager entityManager;

    public List getCoinInfoDao(int pageNum) {
       // Query q = entityManager.createNativeQuery("SELECT * FROM COIN_INFO LIMIT "+100 * (pageNum - 1) + ", 100 FOR JSON AUTO");
        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(CoinInfo.class);
        criteria.setFirstResult(100 * (pageNum - 1)).setMaxResults(100);
        return criteria.list();
    }

    @Transactional
    public void save(CoinInfo coinInfo){
        entityManager.persist(coinInfo);
    }

    @Transactional
    public void dropTable(String tableName) {
        String sql = "TRUNCATE" + " table "+ tableName + ";";
        entityManager.createNativeQuery(sql).executeUpdate();
    }
}
