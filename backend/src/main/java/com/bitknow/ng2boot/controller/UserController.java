package com.bitknow.ng2boot.controller;

import java.util.List;

import com.google.gson.JsonArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.bitknow.ng2boot.model.UserDetails;
import com.bitknow.ng2boot.service.UserService;
import com.bitknow.ng2boot.service.CoinmarketService;
;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private  CoinmarketService coinmarketService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity userDetails() {


        List userDetails = userService.getUserDetails();
        return new ResponseEntity(userDetails, HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public void saveUserDetails() {
        UserDetails detail = new UserDetails();
        detail.setEmail("huhaoshi10000@gmail.com");
        detail.setFirstName("haoshi");
        detail.setLastName("hu");
        detail.setPassword("123");
        userService.save(detail);
    }
    @CrossOrigin
    @RequestMapping(value = "/price", method = RequestMethod.GET)
    public ResponseEntity getTop100(@RequestParam int pageNum) {
        List coinInfo = coinmarketService.getPrice(pageNum);
        return new ResponseEntity(coinInfo, HttpStatus.OK);
    }

//    @GetMapping("/price/{pageNum}")
//    public ResponseEntity test(@PathVariable int pageNum) {
//        List coinInfo = coinmarketService.getPrice(pageNum);
//        return new ResponseEntity(coinInfo, HttpStatus.OK);
//    }

}
