package com.bitknow.ng2boot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bitknow.ng2boot.dao.UserDao;
import com.bitknow.ng2boot.model.UserDetails;


@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public List getUserDetails() {
        return userDao.getUserDetails();
    }

    public void save(UserDetails userDetails) {
        userDao.save(userDetails);
    }

}
