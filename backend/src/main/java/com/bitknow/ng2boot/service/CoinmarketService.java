package com.bitknow.ng2boot.service;


import com.bitknow.ng2boot.dao.CoinInfoDao;
import com.bitknow.ng2boot.dao.UserDao;
import com.bitknow.ng2boot.model.CoinInfo;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.google.gson.stream.JsonReader;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.io.InputStream;
import java.io.*;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

@Service
public class CoinmarketService {

    @Autowired
    private CoinInfoDao coinInfoDao;

    @Autowired
    private UserDao userDao;

    public void savePrice() {
        HttpClient httpClient = new DefaultHttpClient();

        coinInfoDao.dropTable("COIN_INFO");

        try {

            HttpGet httpGetRequest = new HttpGet("https://api.coinmarketcap.com/v1/ticker/?limit=2000&convert=CNY");

            // Execute HTTP request
            HttpResponse httpResponse = httpClient.execute(httpGetRequest);

            System.out.println("----------------------------------------");
            System.out.println(httpResponse.getStatusLine());
            System.out.println("----------------------------------------");

            // Get hold of the response entity
            HttpEntity entity = httpResponse.getEntity();

            JsonArray jsonArray = null;
            if (entity != null) {
                String jsonStr = EntityUtils.toString(entity);
                // parsing JSON
                JsonParser parser = new JsonParser();
                jsonArray = parser.parse(jsonStr).getAsJsonArray();
                for (int i = 0; i < jsonArray.size(); i++) {
                    CoinInfo info  = new CoinInfo();
                    JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();

                    if (!jsonObject.get("name").isJsonNull()) {
                        info.setName(jsonObject.get("name").getAsString());
                    }
                    if (!jsonObject.get("market_cap_usd").isJsonNull()) {
                        info.setMarket_cap_usd(jsonObject.get("market_cap_usd").getAsString());
                    }
                    if (!jsonObject.get("symbol").isJsonNull()) {
                        info.setSymbol(jsonObject.get("symbol").getAsString());
                    }
                    if (!jsonObject.get("rank").isJsonNull()) {
                    info.setRank(jsonObject.get("rank").getAsString());
                    }
                    if (!jsonObject.get("price_usd").isJsonNull()) {
                        info.setPrice_usd(jsonObject.get("price_usd").getAsString());
                    }
                    if (!jsonObject.get("price_btc").isJsonNull()) {
                        info.setPrice_btc(jsonObject.get("price_btc").getAsString());
                    }
                    if (!jsonObject.get("24h_volume_usd").isJsonNull()) {
                        info.set_24h_volume_usd(jsonObject.get("24h_volume_usd").getAsString());
                    }
                    if (!jsonObject.get("available_supply").isJsonNull()) {
                        info.setAvailable_supply(jsonObject.get("available_supply").getAsString());
                    }
                    if (!jsonObject.get("total_supply").isJsonNull()) {
                        info.setTotal_supply(jsonObject.get("total_supply").getAsString());
                    }
                    if (!jsonObject.get("percent_change_1h").isJsonNull()) {
                        info.setPercent_change_1h(jsonObject.get("percent_change_1h").getAsString());
                    }
                    if (!jsonObject.get("percent_change_24h").isJsonNull()) {
                        info.setPercent_change_24h(jsonObject.get("percent_change_24h").getAsString());
                    }
                    if (!jsonObject.get("percent_change_7d").isJsonNull()){
                        info.setPercent_change_7d(jsonObject.get("percent_change_7d").getAsString());
                    }
                    if (!jsonObject.get("last_updated").isJsonNull()) {
                        info.setLast_updated(jsonObject.get("last_updated").getAsString());
                    }
                    if (!jsonObject.get("price_cny").isJsonNull()) {
                        double price = Double.parseDouble(jsonObject.get("price_cny").getAsString());
                        info.setPrice_cny(String.format("%.2f", price));
                    }
                    if (!jsonObject.get("24h_volume_cny").isJsonNull()) {
                        info.set_24h_volume_cny(jsonObject.get("24h_volume_cny").getAsString());
                    }
                    if (!jsonObject.get("market_cap_cny").isJsonNull()) {
                        info.setMarket_cap_cny(jsonObject.get("market_cap_cny").getAsString());
                    }
                    coinInfoDao.save(info);
                }

            }

        } catch (ClientProtocolException e) {
            // thrown by httpClient.execute(httpGetRequest)
            e.printStackTrace();
        } catch (IOException e) {
            // thrown by entity.getContent();
            e.printStackTrace();
        } catch (UnsupportedOperationException e){
            e.printStackTrace();
        }finally {
            httpClient.getConnectionManager().shutdown();
        }
    }

    public List getPrice(int pageNum) {
        if (coinInfoDao.getCoinInfoDao(1).size() == 0)
            savePrice();
        return coinInfoDao.getCoinInfoDao(pageNum);
    }

}


